"""first

Revision ID: 5b9627535ecc
Revises: 
Create Date: 2021-04-17 22:38:12.942873

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.orm import sessionmaker


# revision identifiers, used by Alembic.

revision = '5b9627535ecc'
down_revision = None
branch_labels = None
depends_on = None

Session = sessionmaker()


def upgrade():
    bind = op.get_bind()
    session = Session(bind=bind)
    session.execute(
        """
        CREATE OR REPLACE FUNCTION trigger_set_timestamp()
        RETURNS TRIGGER AS $$
        BEGIN
            NEW.updated_at = NOW();
            RETURN NEW;
        END;
        $$ LANGUAGE plpgsql;
        """
    )


def downgrade():
    bind = op.get_bind()
    session = Session(bind=bind)
    session.execute("DROP FUNCTION IF EXISTS trigger_set_timestamp();")
    pass
