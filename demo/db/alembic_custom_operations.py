from alembic.autogenerate import comparators, renderers
from alembic.operations import Operations, MigrateOperation
from alembic.operations.ops import CreateTableOp


@Operations.register_operation("create_updated_at_trigger")
class CreateUpdatedAtTrigger(MigrateOperation):
    """
    Custom Alembic operation for adding a trigger to a table that updates
    updated_at column of said table on update.
    """

    def __init__(self, trigger_name, table, column, schema=None):
        self.trigger_name = trigger_name
        self.table = table
        self.column = column
        self.schema = schema

    @classmethod
    def create_updated_at_trigger(cls, operations, trigger_name, table, column, schema=None):
        # this classmethod needs to be called the same as the thing
        # you're registering for operations in the decorator up there ^
        op = CreateUpdatedAtTrigger(trigger_name, table, column, schema)
        return operations.invoke(op)

    def reverse(self):
        # reverse provides support for autogeneration
        return DropUpdatedAtTrigger(self.trigger_name, self.table, self.column, schema=self.schema)


@Operations.register_operation("drop_updated_at_trigger")
class DropUpdatedAtTrigger(MigrateOperation):
    """
    Custom Alembic operation to drop updated_at updating trigger from table.
    """

    def __init__(self, trigger_name, table, column, schema=None):
        self.trigger_name = trigger_name
        self.table = table
        self.column = column
        self.schema = schema

    @classmethod
    def drop_updated_at_trigger(cls, operations, trigger_name, table, column, schema):
        # this classmethod needs to be called the same as the thing
        # you're registering for operations in the decorator up there ^
        op = DropUpdatedAtTrigger(trigger_name, table, column, schema=schema)
        return operations.invoke(op)

    def reverse(self):
        # reverse provides support for autogeneration
        return CreateUpdatedAtTrigger(self.trigger_name, self.table, self.column, schema=self.schema)


@Operations.implementation_for(CreateUpdatedAtTrigger)
def create_updated_at_trigger_foo(operations, operation):
    # name of the function that implements an operation does not matter
    # this is what is eventually run when op.create_updated_at_trigger() is called from migration.
    # operation here is CreateUpdatedAtTrigger object
    operations.execute(
        f"""CREATE TRIGGER {operation.trigger_name}
            BEFORE UPDATE ON {operation.schema}.{operation.table}
            FOR EACH ROW
            EXECUTE PROCEDURE trigger_set_timestamp();""")


@Operations.implementation_for(DropUpdatedAtTrigger)
def drop_updated_at_trigger(operations, operation):
    operations.execute(f"DROP TRIGGER {operation.trigger_name} on {operation.schema}.{operation.table}")


@comparators.dispatch_for('schema')
def compare_updated_at_triggers(autogen_context, upgrade_ops, schemas):
    # this is responsible for generating CreateUpdatedAtTrigger and DropUpdatedAtTrigger objects and adding them to
    # upgrade_ops.ops list

    # get to be created tables
    tables_to_be_created = set(
        [(op.table_name, op.schema) for op in upgrade_ops.ops if isinstance(op, CreateTableOp)]
    )

    # issue create trigger operations
    for table, schema in tables_to_be_created:
        schema = schema if schema else autogen_context.dialect.default_schema_name
        upgrade_ops.ops.append(
            CreateUpdatedAtTrigger(
                'set_updated_at',
                table,
                'updated_at',
                schema=schema)
        )


@renderers.dispatch_for(CreateUpdatedAtTrigger)
def render_create_updated_at_trigger(autogen_context, op):
    # this is responsible for writing the operation line into migration
    # op here again is CreateUpdatedAtTrigger object
    return f"op.create_updated_at_trigger('{op.trigger_name}', '{op.table}', '{op.column}', schema='{op.schema}')"


@renderers.dispatch_for(DropUpdatedAtTrigger)
def render_drop_updated_at_trigger(autogen_context, op):
    # this is responsible for writing the operation line into migration
    # while not strictly necessary, since triggers associated with tables are dropped when table is dropped
    # migration path is broken if this doesn't generate something sane.
    # could probably forego this function completely maybe? *shrug*
    return f"op.drop_updated_at_trigger('{op.trigger_name}', '{op.table}', '{op.column}', schema='{op.schema}')"
