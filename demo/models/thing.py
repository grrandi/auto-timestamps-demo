from sqlalchemy import ForeignKey, Integer, Column, String
from sqlalchemy.orm import relationship

from demo.db.base_class import Base


class Thing(Base):
    owner_id = Column(Integer, ForeignKey('user.id', ondelete='CASCADE'))
    owner = relationship('User', back_populates='things')
    name = Column(String)
    description = Column(String)
