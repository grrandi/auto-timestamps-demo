from sqlalchemy import String, Column
from sqlalchemy.orm import relationship

from demo.db.base_class import Base


class User(Base):
    email = Column(String, unique=True, index=True, nullable=False)
    things = relationship('Thing', back_populates='owner')
